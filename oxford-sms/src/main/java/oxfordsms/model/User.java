package oxfordsms.model;

public class User {
	
	private Integer id;
	private String username;
	private String email;
	private String password;
	private String role;
	private int mobilenumber;
	
	//Super constructor	
	public User() {
		super();
	}
	
	//id constructor
	public User(Integer id) {
		super();
		this.id = id;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public int getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(int mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", email=" + email + ", password=" + password + ", role="
				+ role + ", mobilenumber=" + mobilenumber + ", getId()=" + getId() + ", getUsername()=" + getUsername()
				+ ", getEmail()=" + getEmail() + ", getPassword()=" + getPassword() + ", getMobilenumber()="
				+ getMobilenumber() + ", getRole()=" + getRole() + "]";
	}

}
