package oxfordsms.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import oxfordsms.model.User;



@Repository
public class UserDaoImpl implements UserDao{

	
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws DataAccessException {
		
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	
	
	public List<User> listAllUsers() {
		  
		String sql ="SELECT id, username, emailaddress, mobilenumber,password, role from users";
		
		List<User> list = namedParameterJdbcTemplate.query(sql, getSqlParameterByModel(null), new UserMapper());
		return list;
	}
	
	
	private SqlParameterSource getSqlParameterByModel(User user) {
		
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		if(user != null) {
			paramSource.addValue("id", user.getId());
			paramSource.addValue("username", user.getUsername());
			paramSource.addValue("emailaddress", user.getEmail());
			paramSource.addValue("mobilenumber", user.getMobilenumber());
			paramSource.addValue("role", user.getRole());
		}
		
		
		return paramSource;
	}
	
	private static final class UserMapper  implements RowMapper<User>{

		
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setId(rs.getInt("id"));
			user.setUsername(rs.getString("username"));
			user.setEmail(rs.getString("emailaddress"));
			user.setMobilenumber(rs.getInt("mobilenumber"));
			user.setRole(rs.getString("role"));
			
			return user;
		}
		
	}

	@Override
	public void addUser(User user) {
		
		String sql = "INSERT INTO users(username, emailaddress, password, mobilenumber, role) VALUES(:username, :emailaddress, :password, :mobilenumber, :role)";
		
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(user));
	
	
	}

	@Override
	public void updateUser(User user) {
		
		String sql = "update users SET username = :username, emailaddress= :emailaddress, password= :password, mobilenumber= :mobilenumber, role= :role";
		
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(user));
	}

	
	public void deleteUser(int id) {
		String sql = "DELETE from users WHERE id = :id";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(new User(id)));
		
	}

	@Override
	public User findUserById(int id) {
		String sql = "select * from users WHERE id = :id";
		
		return namedParameterJdbcTemplate.queryForObject(sql, getSqlParameterByModel(new User(id)), new UserMapper());
	}





	
}
