package oxfordsms.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import oxfordsms.service.UserService;
import oxfordsms.model.User;

@Controller
@RequestMapping(value="/user")
public class UserController {

	@Autowired
	UserService userservice;
	
	//list of the users
	@RequestMapping(value = "/list",method= RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView model = new ModelAndView("user/user_page");
		List<User> list = userservice.listAllUsers();
		model.addObject("listUser", list);		
		return model;
	}
	//add the record
	@RequestMapping(value = "/add",method= RequestMethod.GET)
	public ModelAndView add(){
		ModelAndView model = new ModelAndView("user/user_form");
		User user = new User();
		model.addObject("userForm", user);		
		return model;
	}
	
	//update the record
	@RequestMapping(value = "/update/{id}",method= RequestMethod.GET)
	public ModelAndView update(@PathVariable("id")int id){
		ModelAndView model = new ModelAndView("user/user_form");
		User user = userservice.findUserById(id);
		model.addObject("userForm", user);		
		return model;
	}
	
	//save the record
	@RequestMapping(value = "/save",method= RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("userForm")User user){
		ModelAndView model = new ModelAndView("user/user_form");
		if(user != null && user.getId() != null) {
			//update
			userservice.updateUser(user);
		}else {
			//Add user
			userservice.addUser(user);
		}		
		return new ModelAndView("redirect:/list");
	}
	
	//Delete the record
	@RequestMapping(value = "/delete/{id}",method= RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id")int id){		
		userservice.deleteUser(id);			
		return new ModelAndView("redirect:/list");
	}
	

}
