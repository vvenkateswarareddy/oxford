<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Footer page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="ClassWork Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
      SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script>
      addEventListener("load", function () {
      	setTimeout(hideURLbar, 0);
      }, false);
      
      function hideURLbar() {
      	window.scrollTo(0, 1);
      }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="resources/masterpage/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <!-- font-awesome icons -->
    <link href="resources/masterpage/css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- //font-awesome icons -->
    <link href="resources/masterpage/css/blast.min.css" rel="stylesheet" type='text/css' media="all" />
    <link rel="stylesheet" type="text/css" href="resources/masterpage/css/style10.css" />
    <!--stylesheets-->
    <link href="resources/masterpage/css/style.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Merriweather:300,400,700,900" rel="stylesheet">
</head>

<body>
    <!--contact -->
    <section class="contact py-lg-4 py-md-3 py-sm-3 py-3" id="contact">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <h3 class="title clr text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">Contact Us</h3>
        <div class="row">
          <div class="col-md-5 address-grid">
            <div class="addres-office-hour text-center" >
              <ul>
                <li class="mb-2">
                  <h6 data-blast="color">Address</h6>
                </li>
                <li>
                  <p>Melbourne,south Brisbane,<br>QLD 4101,Aurstralia.</p>
                </li>
              </ul>
              <ul>
                <li class="mt-lg-4 mt-3">
                  <h6 data-blast="color">Phone</h6>
                </li>
                <li class="mt-2">
                  <p>+ 1 (234) 567 8901</p>
                </li>
                <li class="mt-lg-4 mt-3">
                  <h6 data-blast="color">Email</h6>
                </li>
                <li class="mt-2">
                  <p><a href="mailto:info@example.com">info@example.com</a></p>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-7 contact-form">
            <form action="#" method="post">
              <div class="row text-center contact-wls-detail">
                <div class="col-md-6 form-group contact-forms">
                  <input type="text" class="form-control" placeholder="Your Name" required="">
                </div>
                <div class="col-md-6 form-group contact-forms">
                  <input type="email" class="form-control" placeholder="Your Email" required="">
                </div>
              </div>
              <div class="form-group contact-forms">
                <input type="text" class="form-control" placeholder="Subject" required="">
              </div>
              <div class="form-group contact-forms">
                <textarea class="form-control" rows="3" placeholder="Your Message" required=""></textarea>
              </div>
              <div class="sent-butnn text-center">
                <button type="submit" class="btn btn-block" data-blast="bgColor">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!--//contact -->
    <!--footer-->
    <section class="footer py-lg-4 py-md-3 py-sm-3 py-3">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <div class="row ">
          <div class="dance-agile-info col-lg-3 col-md-6 col-sm-6">
            <h4 class="pb-lg-3 pb-3" data-blast="color">Blog</h4>
            <div class="footer-grid row mb-3">
              <div class="col-lg-5 col-md-6 col-sm-6 col-5">
                <img src="resources/masterpage/images/bb1.jpg" alt=" " class="img-fluid">
              </div>
              <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="#" data-toggle="modal" data-target="#exampleModalLive">ultricies nec,pellen eu,pretium quis,</a></h6>
                <div class="news-bloger">
                  <ul>
                    <li class="mr-1"><a href="#about" class="scroll" data-blast="color">12/4/2019</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">3 Tags</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="footer-grid row mb-3">
              <div class="col-lg-5 col-md-6 col-sm-6 col-5">
                <img src="resources/masterpage/images/bb2.jpg" alt=" " class="img-fluid">
              </div>
              <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="#" data-toggle="modal" data-target="#exampleModalLive">ultricies nec,pellen eu,pretium quis,</a></h6>
                <div class="news-bloger">
                  <ul>
                    <li class="mr-1"><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">12/4/2019</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">3 Tags</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="footer-grid row">
              <div class="col-lg-5 col-md-6 col-sm-6 col-5">
                <img src="resources/masterpage/images/bb3.jpg" alt=" " class="img-fluid">
              </div>
              <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="#" data-toggle="modal" data-target="#exampleModalLive">ultricies nec,pellen eu,pretium quis,</a></h6>
                <div class="news-bloger">
                  <ul>
                    <li class="mr-1"><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">12/4/2019</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">3 Tags</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="dance-agile-info col-lg-3 col-md-6 col-sm-6">
            <h4 class="pb-lg-4 pb-md-3 pb-3 text-uppercase" data-blast="color">Contact Us</h4>
            <div class="address_mail_footer_grids">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1354.295443037977!2d80.03065460277742!3d16.244791941026765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x12dab946010ab386!2sOxford+Concept+School!5e0!3m2!1sen!2sin!4v1550221518620" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
          <div class="dance-agile-info col-lg-3 col-md-6 col-sm-6">
            <h4 class="pb-lg-4 pb-md-3 pb-3 text-uppercase" data-blast="color">Twitter Us</h4>
            <div class="footer-office-hour">
              <ul>
                <li>
                  <p>sit amet consectetur adipiscing</p>
                </li>
                <li class="my-1">
                  <p><a href="mailto:info@example.com" data-blast="color">http://example.com</a> sit amet</p>
                </li>
                <li class="mb-lg-4 mb-md-3 mb-3"><span>Posted 3 days ago.</span></li>
                <li>
                  <p>sit amet consectetur adipiscing</p>
                </li>
                <li class="my-1">
                  <p><a href="mailto:info@example.com" data-blast="color">http://mail.com</a> sit amet</p>
                </li>
                <li><span>Posted 3 days ago.</span></li>
              </ul>
            </div>
          </div>
          <div class="dance-agile-info col-lg-3 col-md-6 col-sm-6">
            <h4 class="pb-lg-3 pb-3" data-blast="color">Our Posts</h4>
            <div class="footer-post d-flex mb-2">
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class=" scroll">
                <img src="resources/masterpage/images/f1.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class="scroll">
                <img src="resources/masterpage/images/f2.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1">
                <a href="#blog" class="scroll">
                <img src="resources/masterpage/images/f3.jpg" alt=" " class="img-fluid">
                </a>
              </div>
            </div>
            <div class="footer-post d-flex mb-2">
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class="scroll">
                <img src="resources/masterpage/images/f3.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class=" scroll">
                <img src="resources/masterpage/images/f2.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1">
                <a href="#blog" class=" scroll">
                <img src="resources/masterpage/images/f1.jpg" alt=" " class="img-fluid">
                </a>
              </div>
            </div>
            <div class="footer-post d-flex">
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class="scroll">
                <img src="resources/masterpage/images/f2.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class=" scroll">
                <img src="resources/masterpage/images/f3.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1">
                <a href="#blog" class=" scroll">
                <img src="resources/masterpage/images/f1.jpg" alt=" " class="img-fluid">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="nav-footer py-sm-4 py-3">
      <div class="container-fluid">
        <div class="buttom-nav ">
          <nav class="border-line py-2">
            <ul class="nav justify-content-center">
              <li class="nav-item active">
                <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="#about" class="nav-link scroll">About</a>
              </li>
              <li class="nav-item">
                <a href="#service" class="nav-link scroll">Services</a>
              </li>
              <li class="nav-item">
                <a href="#blog" class="nav-link scroll">Blog</a>
              </li>
              <li class="nav-item">
                <a href="#contact" class="nav-link scroll">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <footer class="py-3">
      <div class="container">
        <div class="copy-agile-right text-center">
          <div class="list-social-icons text-center py-lg-4 py-md-3 py-3">
            <ul>
              <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
              <li><a href="#"><span class="fas fa-envelope"></span></a></li>
              <li><a href="#"><span class="fas fa-rss"></span></a></li>
              <li><a href="#"><span class="fab fa-vk"></span></a></li>
            </ul>
          </div>
          <p> 
            � 2018 Oxford IIT Concept Education. All Rights Reserved | Design by <a href="http://www.W3Layouts.com" target="_blank">Oxford IIT Concept Education</a>
          </p>
        </div>
      </div>
    </footer>
    <!--//footer-->
    <!--model-->
    <div id="exampleModalLive" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLiveLabel" data-blast="color">ClassWork</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <img src="resources/masterpage/images/b2.jpg" alt="" class="img-fluid">
            <p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae,
              eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellu
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
    <!--//model-->
    <!--js working-->
    <script src='resources/masterpage/js/jquery-2.2.3.min.js'></script>
    <!--//js working--> 
    <!--blast colors change-->
    <script src="resources/masterpage/js/blast.min.js"></script>
    <!--//blast colors change-->
    <!--responsiveslides banner-->
    <script src="resources/masterpage/js/responsiveslides.min.js"></script>
    <script>
      // You can also use "$(window).load(function() {"
      $(function () {
      	// Slideshow 4
      	$("#slider4").responsiveSlides({
      		auto: true,
      		pager:false,
      		nav:true ,
      		speed: 900,
      		namespace: "callbacks",
      		before: function () {
      			$('.events').append("<li>before event fired.</li>");
      		},
      		after: function () {
      			$('.events').append("<li>after event fired.</li>");
      		}
      	});
      
      });
    </script>
    <!--// responsiveslides banner-->		  
    <!--responsive tabs-->	 
    <script src="resources/masterpage/js/easy-responsive-tabs.js"></script>
    <script>
      $(document).ready(function () {
      $('#horizontalTab').easyResponsiveTabs({
      type: 'default', //Types: default, vertical, accordion           
      width: 'auto', //auto or any width like 600px
      fit: true,   // 100% fit in a container
      closed: 'accordion', // Start closed if in accordion view
      activate: function(event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#tabInfo');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
      }
      });
      });
       
    </script>
    <!--// responsive tabs-->	
    <!--About OnScroll-Number-Increase-JavaScript -->
    <script src="resources/masterpage/js/jquery.waypoints.min.js"></script>
    <script src="resources/masterpage/js/jquery.countup.js"></script>
    <script>
      $('.counter').countUp();
    </script>
    <!-- //OnScroll-Number-Increase-JavaScript -->	  
    <!-- start-smoth-scrolling -->
    <script src="resources/masterpage/js/move-top.js"></script>
    <script src="resources/masterpage/js/easing.js"></script>
    <script>
      jQuery(document).ready(function ($) {
      	$(".scroll").click(function (event) {
      		event.preventDefault();
      		$('html,body').animate({
      			scrollTop: $(this.hash).offset().top
      		}, 900);
      	});
      });
    </script>
    <!-- start-smoth-scrolling -->
    <!-- here stars scrolling icon -->
    <script>
      $(document).ready(function () {
      
      	var defaults = {
      		containerID: 'toTop', // fading element id
      		containerHoverID: 'toTopHover', // fading element hover id
      		scrollSpeed: 1200,
      		easingType: 'linear'
      	};
      
      
      	$().UItoTop({
      		easingType: 'easeOutQuart'
      	});
      
      });
    </script>
    <!-- //here ends scrolling icon -->
    <!--bootstrap working-->
    <script src="resources/masterpage/js/bootstrap.min.js"></script>
    <!-- //bootstrap working-->
</body>
</html>