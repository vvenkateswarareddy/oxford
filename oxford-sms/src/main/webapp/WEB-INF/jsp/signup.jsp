
<!DOCTYPE HTML>
<html lang="zxx">

<head>
<title>Validator Signup Form Responsive Widget Template</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8" />
<meta name="keywords"
	content="Validator Signup Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script>
	addEventListener("load", function() {
		setTimeout(hideURLbar, 0);
	}, false);

	function hideURLbar() {
		window.scrollTo(0, 1);
	}
</script>
<!-- Meta tag Keywords -->
<!-- css file -->
<link rel="stylesheet" href="resources/signuppage/css/style.css"
	type="text/css" media="all" />
<!-- Style-CSS -->
<!-- //css file -->
<!-- web-fonts -->
<link
	href="//fonts.googleapis.com/css?family=Cuprum:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese"
	rel="stylesheet">
<!-- //web-fonts -->

</head>

<body>
	<!-- title -->
	<h1>
		<span>V</span>alidator <span>S</span>ignup <span>F</span>orm
	</h1>
	<!-- //title -->
	<!-- content -->
	<div class="sub-main-validi">
		<form validate="true" action="#" method="post">
			<div class="form-group">
				<label for="exampleInputText">User Name</label> <input type="text"
					class="form-control" name="username" id="username"
					placeholder="Enter Name" required>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Email address</label> <input
					type="email" class="form-control" name="emailaddress"
					id="emailaddress" aria-describedby="emailHelp"
					placeholder="Enter email" required>
			</div>
			<div class="form-group">
				<label for="exampleMobile1">Mobile Number</label> <input
					type="mobile" class="form-control" name="mobilenumber"
					id="emailaddress" placeholder="Mobile number" required>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Password</label> <input
					maxlength="10" minlength="3" type="password" name="password"
					class="form-control" id="password" placeholder="Password" required>
			</div>
			<div class="form-group">
				<label for="exampleConfirmPassword1">Confirm Password</label> <input
					type="password" class="form-control" id="confirmpassword"
					name="confirmpassword" placeholder="Confirm Password" required
					data-match="password" data-match-field="#password" />

			</div>
			
			
			<div class="form-group" >
			<label for= class="form-control">Role</label>
				<select class="form-control" required>
					<option class="form-control"  value="0">Select Role:</option>
					<option class="form-control" value="1">Admin</option>
					<option class="form-control" value="2">Staff</option>
					<option class="form-control" value="3">Parent</option>
					<option class="form-control" value="4">Student</option>
				</select>
			</div>

			<div class="form-group">
				<label class="checkbox-inline"> <input type="checkbox"
					value="true" required>I agree to the terms and conditions
				</label>
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
	<!-- //content -->

	<!-- copyright -->
	<div class="footer">
		<p>&copy; 2018 Oxford Concept Schools. All rights reserved.</p>
	</div>
	<!-- //copyright -->

	<!-- Jquery -->
	<script src="resources/signuppage/js/jquery-2.2.3.min.js"></script>
	<!-- //Jquery -->
	<!-- Jquery -->
	<script src="resources/signuppage/js/jquery-simple-validator.min.js"></script>
	<!-- //Jquery -->

</body>

</html>